/* jshint devel:true */

var _ = R;


var get = _.curry(function(x,o){ return o[x]; });


var controls = {
    "spouts" : $('.select--spout ul'),
    "handles": $('.select--handle ul'),
    "finish": $('.select--finish ul')
};

var faucet = {
    "spout":"" ,
    "handle": "",
    "color": ""
}

var path = get('-path');
var selectImages = _.compose(_.keys, get('select'));
var fullImages = _.compose(_.keys, get('full'));

var entry = function(link) {
    var entry = link;
    return '<li class="select--entry"><img src="'+ entry +'"></li>';
}

var appendList = _.curry(function(target, element) {
    target.append(element);
});

// Data Request
var request = $.getJSON('data/image-data.json', function(json){
        return json;
});



var images = images || {};

request.complete(function(){
        images =  request.responseText;
        images = JSON.parse(images);
        App();
});

function buildEntry(list) {
    var imagePath = path(list) + '/select/';
    var images = selectImages(list);
    var links = _.map(_.add(imagePath), images);
    var entries = _.map(entry, links);
    return entries;
}

function updateFaucet(el) {
    var link = el.target.src;
    if(link.indexOf('handles') >= 0) {
        faucet.handles = link.replace('select', 'full').replace("_sniglehandle_small", '').replace("_side", '').replace("_handle", "_handles");
        $('.df--faucet__handles').addClass('hidden');    
        setTimeout(function(){
            $('.df--faucet__handles.hidden').attr('src', faucet.handles);
        }, 400);
        setTimeout(function(){
            $('.df--faucet__handles').removeClass('hidden')
        }, 600);
    }
    if(link.indexOf('spouts') >= 0 && link !== faucet.spout) {
        faucet.spouts = link.replace('select', 'full').replace('_small', '');
        $('.df--faucet__spout').addClass('hidden');    
        setTimeout(function(){
            $('.df--faucet__spout.hidden').attr('src', faucet.spouts);
        }, 400);
        setTimeout(function(){
            $('.df--faucet__spout').removeClass('hidden')
        }, 600);
    
    }
}

var App = function(){
    var spouts = buildEntry(images.spouts); 
    var handles = buildEntry(images.handles);

    spouts.map(appendList(controls.spouts));
    handles.map(appendList(controls.handles));

    $('.select--entry').on('click', updateFaucet)
};

